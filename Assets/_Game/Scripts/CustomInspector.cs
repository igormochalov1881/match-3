using _Game.Scripts.Board;
using UnityEditor;
using UnityEngine;

namespace _Game.Scripts
{
	[CustomEditor(typeof(BoardGenerator))]
	public class CustomInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			BoardGenerator boardGenerator = (BoardGenerator)target;

			if (GUILayout.Button("Generate new board"))
				boardGenerator.GenerateBoardWithButton();
		}
	}
}