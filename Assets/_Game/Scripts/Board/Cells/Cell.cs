using _Game.Scripts.Fruits;
using UnityEngine;

namespace _Game.Scripts.Board.Cells
{
	public class Cell : MonoBehaviour
	{
		private Fruit containedFruit;
		private int id;

		public Fruit ContainedFruit => containedFruit;
		public int Id => id;
		public bool IsFruitContained => ContainedFruit != null;

		public void SetId(int newId) => 
			id = newId;

		public void PutNewFruit(Fruit newFruit)
		{
			if (IsFruitContained == false) 
				containedFruit = newFruit;
			else
				Debug.Log($"Fruit can't be added to cell with id {Id}");
		}

		public void DestroyFruit() => 
			containedFruit = null;
	}
}
