using _Game.Scripts.Board.Cells;
using _Game.Scripts.Fruits;
using UnityEngine;

namespace _Game.Scripts.Board
{
	public class GameBoard : MonoBehaviour
	{
		private Cell[] board;

		public int BoardCellAmount => board.Length;

		public void SetNewBoard(Cell[] newBoard) =>
			board = newBoard;

		public Vector2 GetCellPosition(int cellIndex) => 
			board[cellIndex].gameObject.transform.position;

		public bool CheckIfCellHasFruit(int cellIndex) => 
			board[cellIndex].IsFruitContained;

		public void FillCellWithFruit(int cellIndex, Fruit fruit) => 
			board[cellIndex].PutNewFruit(fruit);

		public void DestroyFruitFromCell(int cellIndex) =>
			board[cellIndex].DestroyFruit();

		public void SetIdsToCells()
		{
			for (int i = 0; i < board.Length; i++)
				board[i].SetId(i);
		}
	}
}