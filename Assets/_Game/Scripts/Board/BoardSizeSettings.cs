using UnityEngine;

namespace _Game.Scripts.Board
{
	public class BoardSizeSettings : MonoBehaviour
	{
		[SerializeField] private int _columns;
		[SerializeField] private int _raws;
		
		/// <summary>
		/// Game board coulumns amount
		/// </summary>
		public int Columns => _columns;
		/// <summary>
		/// Game board raws amount
		/// </summary>
		public int Raws => _raws;
	}
}