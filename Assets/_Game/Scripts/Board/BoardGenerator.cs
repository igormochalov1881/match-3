using _Game.Scripts.Board.Cells;
using UnityEngine;

namespace _Game.Scripts.Board
{
	public class BoardGenerator : MonoBehaviour
	{
		[SerializeField] private GameBoard _board;
		[SerializeField] private BoardSizeSettings _boardSizeSettings;
		[SerializeField] private Cell _lightCellPrefab;
		[SerializeField] private Cell _darkCellPrefab;
		[SerializeField] private Transform _startPosition;
		[SerializeField] private Transform _parentForCells;

		private readonly float defaultCellSize = 1f;

		private void Awake()
		{
			if (CheckIsBoardGenerated() == false)
            {
				_board.SetNewBoard(GenerateBoard());
				_board.SetIdsToCells();
			}
			else
				LinkExistedBoardToScript();
		}

		public void GenerateBoardWithButton()
        {
			if (CheckIsBoardGenerated() == false)
				GenerateBoard();
        }

		public bool CheckIsBoardGenerated()
		{
			if (_board.GetComponentInChildren<Cell>())
				return true;

			return false;
		}

		public Cell[] GenerateBoard()
		{
			Cell[] generatedBoard = new Cell[_boardSizeSettings.Columns * _boardSizeSettings.Raws];
			float cellPositionY = _startPosition.position.y;
			int currentCellId = 0;

			for (int raws = 0; raws < _boardSizeSettings.Raws; raws++)
			{
				float cellPositionX = _startPosition.position.x;

				for (int columns = 0; columns < _boardSizeSettings.Columns; columns++)
				{
					Cell newCell = InstantiateCellPrefab(
						GetCellPrefab(columns, raws),
						new Vector2(cellPositionX, cellPositionY));

					newCell.SetId(currentCellId);
					generatedBoard[currentCellId] = newCell;

					currentCellId++;
					cellPositionX += defaultCellSize;
				}

				cellPositionY += defaultCellSize;
			}

			return generatedBoard;
		}

		private void LinkExistedBoardToScript() 
		{
			_board.SetNewBoard(_board.GetComponentsInChildren<Cell>());
			_board.SetIdsToCells();
		}

		private Cell GetCellPrefab(int columns, int raws)
		{
			bool isLight =	columns % 2 == 1 ? 
							raws % 2 != 0 : 
							raws % 2 != 1;
			
			return isLight ? _lightCellPrefab : _darkCellPrefab;
		}
		
		private Cell InstantiateCellPrefab(Cell cellPrefab, Vector2 position) => 
			Instantiate(cellPrefab, position, Quaternion.identity, _parentForCells);
	}
}