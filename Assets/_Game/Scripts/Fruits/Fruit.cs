using UnityEngine;

namespace _Game.Scripts.Fruits
{
	public class Fruit : MonoBehaviour
	{
		[SerializeField] private FruitType _fruitType;
		[SerializeField] private FruitMovement _fruitMovement;
		
		public FruitType OwnFruit => _fruitType;
		public FruitMovement OwnMovement => _fruitMovement;
	}

	public enum FruitType
	{
		Apple,
		Pear,
		Banana,
		Blueberry,
		Grape
	}
}