using UnityEngine;

namespace _Game.Scripts.Fruits
{
	public class FruitFactory : MonoBehaviour
	{
		[SerializeField] private Fruit[] fruitsPrefabs;
		[SerializeField] private Transform fruitsParent;

		public Fruit SpawnFruit(Vector2 position) => 
			Instantiate(GetRandomFruitPrefab(), position, Quaternion.identity, fruitsParent);

		private Fruit GetRandomFruitPrefab() => 
			fruitsPrefabs[Random.Range(0, fruitsPrefabs.Length)];	
	}
}