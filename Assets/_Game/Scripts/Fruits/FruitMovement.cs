using _Game.Scripts.Board;
using UnityEngine;
using System.Collections;

namespace _Game.Scripts.Fruits
{
	public class FruitMovement : MonoBehaviour
	{
		private readonly float delayBetweenCanFallCheck = 0.5f;

		private Fruit fruit;
		private GameBoard board;
		private int currentCellIndex;
		private int previousCellIndex;
		private int columnsAmount;

		public void Init(GameBoard gameBoard, Fruit thisFruit, int currentCell, int boardColumnsAmount)
		{
			fruit = thisFruit;
			board = gameBoard;
			currentCellIndex = currentCell;
			columnsAmount = boardColumnsAmount;
			FillCurrentCell();
		}

        private void OnMouseDown() => 
			Destroy(gameObject);

        private void FixedUpdate()
		{
			StartCoroutine(FallIfPossible());
		}

		private IEnumerator FallIfPossible()
        {
			yield return new WaitForSeconds(delayBetweenCanFallCheck);

			int cellToFall = currentCellIndex - columnsAmount;

			if (CheckIfCanFall(cellToFall))
			{
				previousCellIndex = currentCellIndex;
				
				Vector2 positionToFall = board.GetCellPosition(cellToFall);

				currentCellIndex = cellToFall;
				transform.position = positionToFall;

				UpdateFruitPositionOnBoard();
			}
		}

		private bool CheckIfCanFall(int cellToCheck) => 
			cellToCheck >= 0 && board.CheckIfCellHasFruit(cellToCheck) == false;

		private void UpdateFruitPositionOnBoard()
		{
			FillCurrentCell();
			DestroyFromPreviousCell();
		}

		private void FillCurrentCell() => 
			board.FillCellWithFruit(currentCellIndex, fruit);

		private void DestroyFromPreviousCell() => 
			board.DestroyFruitFromCell(previousCellIndex);
	}
}