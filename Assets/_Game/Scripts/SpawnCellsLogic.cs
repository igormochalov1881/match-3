using _Game.Scripts.Board;
using _Game.Scripts.Fruits;
using System.Collections;
using UnityEngine;

namespace _Game.Scripts
{
	public class SpawnCellsLogic : MonoBehaviour
	{
		[SerializeField] private FruitFactory _fruitFactory;
		[SerializeField] private GameBoard _gameBoard;
		[SerializeField] private BoardSizeSettings _boardSettings;
		[Range(0.05f, 0.5f)]
		[SerializeField] private float _delayBetweenSpawns;

		private int[] spawningCellsIndexes;
		private bool[] spawningCellsEmptiness;
		private bool isSpawning;

		private void Start()
		{
			spawningCellsIndexes = new int[_boardSettings.Columns];
			spawningCellsEmptiness = new bool[_boardSettings.Columns];
			
			FillIndexesOfSpawningCells();
		}

		private void Update()
		{
			StartCoroutine(CheckSpawningCellsOnFruits());
		}

		private void FillIndexesOfSpawningCells()
		{
			for (int i = 0; i < _boardSettings.Columns; i++)
			{
				spawningCellsIndexes[i] = _gameBoard.BoardCellAmount - (i + 1);
			}
		}
		
		private IEnumerator CheckSpawningCellsOnFruits()
		{
			if (isSpawning)
				yield break;

			for (int i = 0; i < _boardSettings.Columns; i++)
			{
				spawningCellsEmptiness[i] = CheckIfCellHasFruit(spawningCellsIndexes[i]);

				if (spawningCellsEmptiness[i] == false)
				{
					isSpawning = true;

					SpawnFruit(spawningCellsIndexes[i]);
					
					yield return new WaitForSeconds(_delayBetweenSpawns);
				}
			}

			isSpawning = false;
		}

		private void SpawnFruit(int spawnCellIndex)
		{
			Vector2 fruitPosition = _gameBoard.GetCellPosition(spawnCellIndex);

			Fruit spawnedFruit = _fruitFactory.SpawnFruit(fruitPosition);

			spawnedFruit.OwnMovement.Init(_gameBoard, spawnedFruit, spawnCellIndex, _boardSettings.Columns);
		}

		private bool CheckIfCellHasFruit(int cellId) => 
			_gameBoard.CheckIfCellHasFruit(cellId);
	}
}